### Hi there 👋

![](https://github-readme-stats.vercel.app/api?username=algebra-fun&count_private=true&show_icons=true)

![](https://github-readme-stats.vercel.app/api/top-langs/?username=algebra-fun&layout=compact&hide=jupyter%20notebook,less&langs_count=10)
